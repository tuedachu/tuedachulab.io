;;; publish.el --- Build and Org blog -*- lexical-binding: t -*-

;; Copyright (C) 2019 Arnaud Hoffmann <arnaud@tuedachu.xyz>

;; Author: Arnaud Hoffmann <arnaud@tuedachu.xyz>
;; Maintainer: Arnaud Hoffmann <arnaud@tuedachu.xyz>
;; URL: https://gitlab.com/tuedachu/tuedachu.gitlab.io
;; Version: 1.0
;; Keywords: org, website, blog, fuzzy-search

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Inspiration:
;; - https://gitlab.com/pages/org-mode.
;; - ambrevar org website gitlab.com/ambrevar/ambrevar.gitlab.io

(require 'ox-publish)
(add-to-list 'load-path ".")
(require 'htmlize)


;; Include customized scripts
(require 'menu-builder)
(require 'fuzzy-search-feed-builder)
(require 'news-builder)
(require 'datestamp)

(defvar tuedachu/domain
  "https://tuedachu.xyz/"
  "Domain of the website. Add the last '/'.")

(defvar tuedachu/author
  "A. Hoffmann"
  "Name of author")

(defvar tuedachu/root
  (expand-file-name ".")
  "Path to project root")

(defvar tuedachu/repository
  "https://gitlab.com/tuedachu/tuedachu.gitlab.io"
  "URL of repository")

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg" "asc"
                "ico" "cur" "css" "js" "woff" "html" "pdf" "bib"
                "sh" "txt"))
  "File extensions to be igonred by `org-publishing-function'")

(defun tuedachu/postamble (prefix)
  "Return html code of postamble.

PREFIX is a prefix for all links (e.g. '../')."
  (concat "<img style='float:right; height: 150px;vertical-align: middle;' src='"
          prefix
          "tuedachu.PNG'>"
          "<p><b>tuedachu&copy</b><br>"
          "Arnaud Hoffmann<br>"
          "<a href='mailto:mail@tuedachu.xyz'>mail@tuedachu.xyz</a></p>"
          "<p> <a href='https://gitlab.com/tuedachu/tuedachu.gitlab.io/-/issues'>"
          "Report an issue</a></p>"
          "<p><a href='"
          prefix
          "tuedachu.asc'>GPG public key</a><br></p>"
          "<a href='http://creativecommons.org/licenses/by-sa/4.0'><img src='"
          prefix
          "CC.png' style='height:30px;'></a>"))

(defun tuedachu/preamble (prefix)
  "Return html code of preamble.

PREFIX is a prefix for all links (e.g. '../')."
  (concat "<link rel='shortcut icon' type='image/x-icon' href='"
          tuedachu/domain
          "tuedachu-icon.png'>\n"
          "<link rel='icon' type='image/x-icon' href='"
          tuedachu/domain
          "tuedachu-icon.png'\n>"
          "<link rel='stylesheet' type='text/css' href='"
          prefix
          "style.css'>\n"
          "<link rel='stylesheet' type='text/css' href='"
          prefix
          "jQuery.css'>\n"))

(defun tuedachu/search-bar (prefix)
  "Return html code to be included to display a seach bar.

PREFIX is a prefix for all links (e.g. '../')."
  (concat "<input id='search-bar' placeholder='Search anything...'>\n"
          "<script src='https://code.jquery.com/jquery-1.12.4.js'></script>\n"
          "<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>\n"
          "<script src='"
          prefix
          "light-fuzzy-search.js'></script>\n"
          "<script src='"
          prefix
          "data.js'></script>\n"))

(defun tuedachu/preamble-article (info)
  "Return preamble as a string."
  (let* ((file (plist-get info :input-file))
         (prefix (concat
                  (file-relative-name (expand-file-name "org" tuedachu/root)
                                      (file-name-directory file))
                  "/"))
         (creation-date (tuedachu/get-creation-date file))
         (last-commit (tuedachu/get-last-commit file)))
    (concat tuedachu/menu-header
            "\n"
            (tuedachu/search-bar prefix)
            "\n"
            "<script>\n"
            "$(function(){\n"
            "   $('.credits').insertAfter($(document).find('.title'));\n"
            "});\n"
            "</script>\n"
            "<div class='credits'>\n"
            "&mdash; by "
            tuedachu/author
            " on "
            creation-date
            (if (equal creation-date last-commit)
                "."
              (concat " (Last update on "
                      "<a href="
                      tuedachu/repository
                      "/commits/master/"
                      (file-relative-name file tuedachu/root)
                      ">"
                      last-commit
                      "</a>"
                      ")."))
            "</div>")))

;; Start of script
(setq tuedachu/menu-header
      (progn
        (tuedachu/org-create-menu "./menu.org" "./menu.html")
        (with-temp-buffer
          (insert-file-contents "./menu.html")
          (buffer-string))))

(tuedachu/create-json-file "./org/data.js" "./org")

;; Index.html
(tuedachu/parse-news-file "./news.org"
                          "./org/index.html"
                          'tuedachu/print-news-html
                          t
                          (concat "<!DOCTYPE html>\n"
                                  "<html lang='en'>\n"
                                  "<head>\n"
		                  "<meta charset='utf-8'>\n"
	                          "<meta name='viewport' content='width=device-width, initial-scale=1'>\n"
	                          "<title>Tuedachu website</title>\n"
                                  (tuedachu/preamble "")
                                  tuedachu/menu-header
                                  "\n"
                                  (tuedachu/search-bar "")
                                  "\n"
                                  "</head>\n"
                                  "<body>\n"
                                  "<h1 style='text-align:center; font-size: 30pt;'> News </h1>")
                          (concat "<footer id='postamble' class='status'>\n"
                                  (tuedachu/postamble "")
	                          "\n</footer>\n"
                                  "</body>\n</html>"))
;; All news files
(tuedachu/parse-news-file "./news.org"
                          "./org/all_news.html"
                          'tuedachu/print-news-html
                          nil
                          (concat "<!DOCTYPE html>\n"
                                  "<html lang='en'>\n"
                                  "<head>\n"
		                  "<meta charset='utf-8'>\n"
	                          "<meta name='viewport' content='width=device-width, initial-scale=1'>\n"
	                          "<title>Tuedachu website</title>\n"
                                  (tuedachu/preamble "")
                                  tuedachu/menu-header
                                  "\n"
                                  (tuedachu/search-bar "")
                                  "\n"
                                  "</head>\n"
                                  "<body>\n"
                                  "<h1 style='text-align:center; font-size: 30pt;'> All News </h1>")
                          (concat "<footer id='postamble' class='status'>\n"
                                  (tuedachu/postamble "")
	                          "\n</footer>\n"
                                  "</body>\n</html>"))


(setq org-publish-use-timestamps-flag t
      org-publish-timestamp-directory "./"
      make-backup-files nil
      org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc t
      org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-postamble t
      org-html-postamble (tuedachu/postamble "../")
      org-html-html5-fancy t
      org-html-doctype "html5"
      org-html-preamble #'tuedachu/preamble-article)

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "./org"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
             :auto-sitemap nil
	     :auto-preamble nil
	     :section-numbers nil
	     ;;:body-only t
	     :html-head-include-default-style nil
	     :html-head-include-scripts nil
             :html-head (tuedachu/preamble "../")
             :sitemap-style 'tree)
       (list "site-static"
             :base-directory "./org"
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site-cert"
             :base-directory ".well-known"
             :exclude "public/"
             :base-extension 'any
             :publishing-directory "./public/.well-known"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org"))))

;; clear cache
(when (file-exists-p "./site-org.cache")
  (delete-file "./site-org.cache"))

(when (file-exists-p "./site-static.cache")
  (delete-file "./site-static.cache"))

(org-publish-all)
