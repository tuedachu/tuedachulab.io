;;; fuzzy-search-feed-builder.el starts here

(require 'find-lisp)

(defun tuedachu/org-get-header-properties (filename properties)
  "Get the values of all header properties in PROPERTIES from org
file FILENAME.

PROPERTIES is a list of string."
  (with-temp-buffer
    (insert (org-file-contents filename))
    (org-mode)
    (org-element-map (org-element-parse-buffer)
        'keyword
      (lambda(el)
        (let ((list-of-prop nil))
          (dolist (prop properties)
            (when (string-match prop (org-element-property :key el))
              (push (org-element-property :value el)
                    list-of-prop)))
          list-of-prop)))))

(defun tuedachu/create-json-file (jsfile directory)
  "Create fuzzy search feed from all org files located in
DIRECTORY and sub-directories."
  (let (file header)
    (with-temp-file jsfile
      (insert "data = [")
      ;;insert home in the list
      (insert (concat "\n{"
                      "title: 'Homepage',\n"
                      "url: 'index.html',\n"
                      "keywords: 'News, Home, Start'\n"
                      "}\n"
                      ","))
      (dolist (file (find-lisp-find-files directory "\\.org$"))
        (unless (string= (file-relative-name file "./org")
                         "404.org")
          (setq header (tuedachu/org-get-header-properties file '("TITLE" "KEYWORDS")))
          (insert (concat "\n{"
                          "title: '"
                          (car (nth 0 header))
                          "',\n"
                          "url: '"
                          (replace-regexp-in-string "\\.org$"
                                                    ".html"
                                                    (file-relative-name file "./org"))
                          "',\n"
                          "keywords: '"
                          (car (nth 1 header))
                          "'\n"
                          "}\n"
                          ","))))
      (move-beginning-of-line nil)
      (kill-line)
      (insert "];"))))

(provide 'fuzzy-search-feed-builder)
;;; fuzzy-search-feed-builder.el ends here
