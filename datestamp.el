;; Inspiration from ambrevar's 'publish.el'
;; see: gitlab.com/ambrevar/ambrevar.gitlab.io

(defun tuedachu/get-creation-date (file)
  "Return the first commit date of FILE
             Format is YYYY-MM-DD."
  (with-temp-buffer
    (call-process "git" nil t nil "log" "--reverse" "--date=short" "--pretty=format:%cd" file)
    (goto-char (point-min))
    (buffer-substring-no-properties (line-beginning-position) (line-end-position))))

(defun tuedachu/get-last-commit (file)
  "Return the last commit date of FILE
             Format is YYYY-MM-DD."
  (with-output-to-string
    (with-current-buffer standard-output
      (call-process "git" nil t nil "log" "-1" "--date=short" "--pretty=format:%cd" file))))

(provide 'datestamp)
