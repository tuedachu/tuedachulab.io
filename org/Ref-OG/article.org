#+TITLE: Oil & Gas References
#+OPTIONS: tex:t
#+KEYWORDS: Oil, Gas, References


* Professor Curtis H. Whitson
  - [[http://www.ipt.ntnu.no/~curtis/courses/][NTNU Course material]]
  - [[https://www.youtube.com/channel/UCDa3gUF-Vk8u02p7obTaImg][YouTube Channel]]
  - [[https://whitson.com/][whitson, the PVT company (PERA)]]

* Professor Milan Stanko
  - [[http://www.ipt.ntnu.no/~stanko/Resources.html][Personal website]]
  - [[https://www.youtube.com/channel/UCWMfsCe1NQMgx4UZWrVvFgA][YouTube Channel]]

* Other Sources
  - [[https://petrowiki.org/PetroWiki][PetroWiki]]
  - [[http://www.ipt.ntnu.no/~kleppe/][Professor Jon Kleppe's website]]
  - [[https://www.journals.elsevier.com/journal-of-petroleum-science-and-engineering][Journal of Petroleum Science and Engineering]]
  - [[https://link.springer.com/journal/volumesAndIssues/13202][Journal of Petroleum Exploration and Production Technology (Open Access)]]

