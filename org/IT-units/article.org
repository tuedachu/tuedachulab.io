#+TITLE: Efficient Implementation of Unit Conversion
#+OPTIONS: tex:t
#+KEYWORDS: Programming, Units, Implementation, Hash tables

* Introduction


  I have been working for many years with engineering topics, and one
  recurring issue is units consistency and unit conversions. Users may
  input the units in one unit system (e.g. SI unit system),
  calculations may use a different unit system and results might be
  output in another unit system.

  Inherently, engineering software applications need unit conversions.

  For many years, I have been struggling with finding the best way to
  implement such unit conversion module/class/functions... Very often,
  I ended up addressing all the cases manually leading to very long
  code and frustration.

  This article presents an alternative that is way more efficient and
  readable. It uses a conversion factor table.


* Proposed Solution Using a Conversion Factor Table

** Main Idea

   Most of units can be converted into another unit using a linear
   expression:

   \begin{equation}
	\mathrm{unit}_2 = a \cdot \mathrm{unit}_1 + b,
	\label{eq:unit-linear}
   \end{equation}

   where $a$ and $b$ are coefficients depending on $\mathrm{unit}_1$
   and $\mathrm{unit}_2$.

** Build a Conversion Factor Table to a Set of Common Basis

   1. For each unit type (e.g. distance, pressure, etc.), a common
      basis is to be chosen. Say meters for distances, bar for
      pressures, Celsius for temperatures, etc.

   2. Create a conversion table from any units to the common basis
      units. Most of modern programming languages implement a hash
      table:
      + =unordered_map= in C++
      + =map= in go
      + =dict= in python
      + hash tables in emacs lisp, common lisp (=make-hask-table=)

      Given a convesion factor table =T=, the key of each entry should
      be the unit name (i.e. a string). The value of each entry should
      be a list of two elements: the first one is the $a$ coefficient
      and the latter is the $b$ coefficient, see
      Eq. (\ref{eq:unit-linear}).

      Here is an example in python using meters as common basis for
      distances and Celsius as common basis for temperatures:
      #+BEGIN_SRC python
table = {}
# distances
table['m'] = [0, 0]  # Common basis
table['km'] = [0.001, 0]
table['ft' = [0,3048, 0]

# temperatures
table['C'] = [0, 0] # Common basis
table['K'] = [0, -273,15]
table['F'] = [1.8, -32/1.8]
      #+END_SRC

** Unit Conversion Implementation

   Let us consider three units:
   - =unit_from= is the original unit
   - =unit_to= is the final unit
   - =unit_c= is the common basis unit

   Applying Eq. (\ref{eq:unit-linear}) to units =unit_c= and
   =unit_from=:

   \begin{equation}
      \mathrm{unit_c} = a(\mathrm{unit_{from}}) \cdot \mathrm{unit_{from}} + b(\mathrm{unit_{from}}).
      \label{eq:unit-from}
   \end{equation}

   Applying Eq. (\ref{eq:unit-linear}) to units =unit_c= and
   =unit_to=:
   \begin{equation}
      \mathrm{unit_c} = a(\mathrm{unit_{to}}) \cdot \mathrm{unit_{to}} + b(\mathrm{unit_{to}}).
   \end{equation}

   In other words:
   \begin{equation}
	 \mathrm{unit_{to}} = \frac{\mathrm{unit_c}}{a(\mathrm{unit_{to}})} - b(\mathrm{unit_{to}}).
	 \label{eq:unit-to}
   \end{equation}


   Now, substituting $\mathrm{unit_c}$ by Eq. (\ref{eq:unit-from}) in
   Eq. (\ref{eq:unit-to}):
   \begin{equation}
      \mathrm{unit_{to}} = \frac{a(\mathrm{unit_{to}}) \cdot \mathrm{unit_{to}} + b(\mathrm{unit_{to}})}{a(\mathrm{unit_{to}})} - b(\mathrm{unit_{to}}).
   \end{equation}


** Alliases

   If units have alliases (e.g. cubic feet can be written cf or ft³)
   then one can implement can first find the common names of
   =unit_from= and =unit_to=. The conversion table only includes
   conversion names from/to common unit names.

   The conversion from unit name to common unit name can also be done
   with a hash table.

** Unit Type

   To make the unit conversion safer, it is a good practice to ensure
   that =unit_from= and =unit_to= represent the same type of quantity
   (e.g. volumes or temperature).



* Example in python

  #+BEGIN_SRC python
from dataclasses import dataclass

class UnphysicalConversionError(Exception):
    def __init__(
        self, from_unit: str, from_unit_type: str, to_unit: str, to_unit_type: str
    ):
        self.from_unit = from_unit
        self.from_unit_type = from_unit_type
        self.to_unit = to_unit
        self.to_unit_type = to_unit_type

    def __str__(self):
        return (
            f"Conversion from {self.from_unit} to {self.to_unit} not allowed."
            f"\nUnphysical conversion from {self.from_unit_type} to {self.to_unit_type}"
        )

class UnitDict(dict):
    def __getitem__(self, unit):
        try:
            return super().__getitem__(unit)
        except KeyError:
            raise InvalidUnitError(unit, allowed_units=self.keys())

@dataclass
class Unit:
    name: str = ""
    unit_type: str = ""


# Unit alliases
common_unit = UnitDict()

# Volumes
common_unit["bbl"] = Unit("bbl", "volume")
common_unit["stb"] = Unit("bbl", "volume")
common_unit["rb"] = Unit("bbl", "volume")
common_unit["m3"] = Unit("m3", "volume")
common_unit["sm3"] = Unit("m3", "volume")
common_unit["cf"] = Unit("cf", "volume")
common_unit["scf"] = Unit("cf", "volume")
common_unit["ft3"] = Unit("cf", "volume")
common_unit["mcf"] = Unit("mcf", "volume")
common_unit["mscf"] = Unit("mcf", "volume")
common_unit["mmcf"] = Unit("mmcf", "volume")
common_unit["mmscf"] = Unit("mmcf", "volume")
common_unit["cm3"] = Unit("cm3", "volume")
common_unit["cc"] = Unit("cm3", "volume")

# Pressures
common_unit["bara"] = Unit("bara", "pressure")
common_unit["bar"] = Unit("bara", "pressure")
common_unit["barg"] = Unit("barg", "pressure")
common_unit["psia"] = Unit("psia", "pressure")
common_unit["psi"] = Unit("psia", "pressure")
common_unit["psig"] = Unit("psig", "pressure")
common_unit["paa"] = Unit("paa", "pressure")
common_unit["pa"] = Unit("paa", "pressure")
common_unit["pag"] = Unit("pag", "pressure")
common_unit["kpaa"] = Unit("kpaa", "pressure")
common_unit["kpa"] = Unit("kpaa", "pressure")
common_unit["kpag"] = Unit("kpag", "pressure")
common_unit["atma"] = Unit("atma", "pressure")
common_unit["atm"] = Unit("atma", "pressure")
common_unit["atmg"] = Unit("atmg", "pressure")

# Temperature
common_unit["c"] = Unit("c", "temperature")
common_unit["f"] = Unit("f", "temperature")
common_unit["r"] = Unit("r", "temperature")
common_unit["k"] = Unit("k", "temperature")

factor_table = UnitDict()

# Pressures
factor_table["bara"] = [1.0, 0.0]  # commmon pressure is bara
factor_table["barg"] = [1.0, 1.01325]
factor_table["psia"] = [1 / 14.50377498, 0.0]
factor_table["psig"] = [1 / 14.50377498, 1.01325]
factor_table["paa"] = [1.0e-5, 0.0]
factor_table["pag"] = [1.0e-5, 1.01325]
factor_table["atma"] = [1.01325, 0.0]
factor_table["atmg"] = [1.01325, 1.01325]
factor_table["kpaa"] = [0.01, 0.0]
factor_table["kpag"] = [0.01, 1.01325]

# Temperatures
factor_table["c"] = [1.0, 0.0]  # common temperature is C
factor_table["f"] = [1.0 / 1.8, -32.0 / 1.8]
factor_table["k"] = [1.0, -273.15]
factor_table["r"] = [1.0 / 1.8, -273.15]

# Volumes
factor_table["m3"] = [1.0, 0.0]  # common volume is m3
factor_table["cf"] = [0.3048 ** 3, 0.0]
factor_table["mcf"] = [1000 * 0.3048 ** 3, 0.0]
factor_table["mmcf"] = [1e6 * 0.3048 ** 3, 0.0]
factor_table["bbl"] = [1 / 6.28981077, 0.0]


def get_common_unit_name(unit: str) -> (str, str):
    unit = unit.lower()
    return common_unit[unit].name, common_unit[unit].unit_type

def convert(value: float, from_unit: str, to_unit: str) -> float:
    """
    Convert value from unit (from_unit) to unit (to_unit) and return the converted value.
    """

    if value is None:
        return None

    to_unit, to_unit_type = get_common_unit_name(to_unit.lower())
    from_unit, from_unit_type = get_common_unit_name(from_unit.lower())

    if to_unit_type != from_unit_type:
        raise UnphysicalConversionError(
            to_unit, to_unit_type, from_unit, from_unit_type
        )

    if from_unit == to_unit:
        return value # No conversion needed

    c_from_unit = factor_table[from_unit]
    c_to_unit = factor_table[to_unit]

    return (value * c_from_unit[0] + c_from_unit[1] - c_to_unit[1]) / c_to_unit[0]
  #+END_SRC

* Numerical Errors

  The proposed solution is suitable for a lot of engineering application
  where round-off errors are not an issue. If your engineering
  application is sensitive to round-off errors, I *strongly* recommend
  using a different solution.


