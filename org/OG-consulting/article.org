#+TITLE: Tuedachu Consulting
#+KEYWORDS: Oil, Petroleum, Gas, Consulting, Study

#+ATTR_HTML: :style display: block ; margin-left: auto; margin-right: auto; width: 30%;
[[../tuedachu-consulting-blue.PNG]]


* What I do

  I offer my consulting services and expertise in different domains:
  - PVT and fluid charactrization (EOS models)
  - Production allocation (compositional tracking)
  - Production system modeling: reservoir - well - network - surface process
  - Integrated Asset Modeling (IAM)
  - Short-term production optimization

  Please contact [[mailto:arnaud@tuedachu.xyz][me]] for further information.


* Example of past projects

  - Sales product forecast for a multi-field gas condensate asset (Egypt)
  - Optimized reservoir-network integration for an offshore field (Angola)
  - Value chain optimization for an onshore multi-field asset (Algeria)
  - Short-term production optimization (Azerbaijan) 
  - Mid-term field development optimization to support the decision to
    install artifical lift (Azerbaijan)
  - Process modeling and optimization (Kazakhstan)
  - Diluent injection optimization for a heavy oil field (UK)
  - Production allocation for a complex multi-field asset (Colombia)
  - Real-time production optimization for a multi-field offshore asset (Brazil)
