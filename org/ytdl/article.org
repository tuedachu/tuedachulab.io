#+TITLE: ytdl: An Emacs Interface for youtube-dl
#+OPTIONS: tex:t
#+KEYWORDS: Emacs, youtube-dl, frugality, elisp


* Motivation

  We live in an extraordinary time where knowledge is available for
  free in numerous forms: e-books, wikis, tutorials and of course
  videos and podcasts. I am a strong advocate for universal access to
  knowledge. However, this comes at a cost that is becoming
  increasingly high, especially when it comes to HD videos published
  on platforms like YouTube.

  One way to minimize our digital footprint is to avoid watching
  twice the same videos. This is probably impossible except for
  people having a perfect memory. The rest of us need to find another
  way to access these pieces of knowledge without downloading again
  and again the same video.

  =youtube-dl= is a great tool for that! It enables downloading
  videos from numerous platforms (including YouTube and
  PeerTube). Files are then located on your local disk and accessible
  on your machine at any time.

  However, like other command-line tools, =youtube-dl= can be
  challenging to use (many arguments, need to use the =--help= command
  or the man page before actually using the tool, etc.). Those
  challenges (or leaning curves) can prevent many potential users from
  using the tool.

  The idea of =ytdl= is to provide an intuitive and flexible Emacs
  interface for =youtube-dl=. =ytdl= leads to considerable gains in
  efficiency and comfort, especially when used with exwm. 

* Installation

  =ytdl= is part of Melpa. To install it:
  - =M-x= =package-install=
  - =ytdl=

  More information [[https://gitlab.com/tuedachu/ytdl][here]].

* Source Code

  =ytdl= is realeased under GPL v3. The source code can be found [[https://gitlab.com/tuedachu/ytdl][here]].


* Demo

  #+ATTR_HTML: :style display: block ; margin-left: auto; margin-right: auto; width: 90%;
  [[./usage.gif]]




  
