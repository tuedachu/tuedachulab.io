$( function() {
    $.ui.autocomplete.filter = function (array, terms) {
        var filters;
        filters = $( "#search-bar" ).val().split(" ").filter(function(x) {
            return (x !== (undefined || ''));
        });
        var list = [];
        for(var i = 0; i < data.length; i++) {
            var display = false;
            if (filters.length > 0) display = true;
            for (var j = 0; j < filters.length; j++) {
		var filter = filters[j].toUpperCase();
		if (data[i].title.toUpperCase().indexOf(filter) == -1 && data[i].keywords.toUpperCase().indexOf(filter) == -1) {
		    display = false;
		    break;
		}
            }
            if(display) list.push(data[i]);
        }
        return list;
    };

    $("#search-bar").autocomplete({
	minLength: 1,
	source: data,
	multiple: true,
	mustMatch: false,
	select: function( event, ui ) {
            console.log( ui.item);
            $( "#search-bar" ).val("");
            OpenPage(ui.item.url);
            return false;
	},
	focus: function( event, ui ) {
            console.log( ui.item);
            return false;
	},
	change: function( event, ui ) {
            console.log( ui.item);
            return false;
	}
    })
	.autocomplete( "instance" )._renderItem = function( ul, item ) {
	    var bar_width = $("#search-bar").innerWidth() - 10;
	    console.log(bar_width);
	    return $( "<li>" )
	    	.append( "<div style='width:" + bar_width + "px; padding: 5px 5px 5px 5px;'>" + "<b>" + item.title + "</b>" + "<br><div class='fs_keyword'>" + item.keywords + "</div></div>" )
		.appendTo( ul );
	};
});

function OpenPage(url){
    window.open("https://tuedachu.xyz/" + url,"_self")
}
