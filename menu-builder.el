;;; menu-builder.el starts here

(defun tuedachu/org-get-header-list (filename org-property-list)
  "Get the properties of ORG-PROPERTY-LIST of each headline from org
file FILENAME as a flat list of headers and levels."
  (with-temp-buffer
    (insert (org-file-contents filename))
    (org-mode)
    (let ((tree (org-element-parse-buffer 'headline)))
      (org-element-map
          tree
          'headline
        (lambda (el)
          (let (l
                prop)
            (dolist (prop org-property-list)
              (setq l (add-to-list 'l (org-element-property prop el))))
            (reverse l)))))))

(defun tuedachu/org-create-menu (menu-org-file menu-html-file)
  (let ((header-list (tuedachu/org-get-header-list menu-org-file
                                                   '(:raw-value :level :URL)))
        (previous-level 1)
        (ul-opened 0)
        current-header
        current-header-title
        current-header-level
        current-header-url)
    (with-temp-file menu-html-file
      (insert "<nav id='menu'>\n<ul>\n")
      (insert (concat "<img style='float:right; height: 45px;padding: 5px 5px 5px 5px;' src='"
                      tuedachu/domain
                      "tuedachu-red.png'>\n"))
      (while header-list
        (setq current-header (car header-list)
              current-header-title (nth 0 current-header)
              current-header-level (nth 1 current-header)
              current-header-url (nth 2 current-header))
        (when (< previous-level current-header-level)
          (insert "<div>\n<ul>\n")
          (setq ul-opened (1+ ul-opened)))
        (while (> previous-level current-header-level)
          (insert "</ul>\n</div></li>\n")
          (setq ul-opened (1- ul-opened)
                previous-level (1- previous-level)))
        (insert (concat "<li><a"
                        (when current-header-url
                          (concat " href='"
                                  (concat tuedachu/domain current-header-url)
                                  "'"))
                        ">"
                        current-header-title
                        (if (and (cdr header-list)
                                 (> (nth 1 (car (cdr header-list))) current-header-level))
                            " <span class='caret'></span></a>"
                          "</a></li>\n")))
        (setq header-list (cdr header-list)
              previous-level current-header-level))
      (dotimes (i current-header-level)
        (insert "</ul>\n")
        (when (> ul-opened 0)
          (setq ul-opened (1- ul-opened))
          (insert "</li>\n")))
      (insert "</nav>\n"))))

(provide 'menu-builder)
;;; menu-builder.el ends here
