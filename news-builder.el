;;; news-builder.el starts here

(require 'menu-builder)

(defun tuedachu/org-get-content-list (filename)
  "Get the contents of org file FILENAME as a list."
  (with-temp-buffer
    (insert (org-file-contents filename))
    (org-mode)
    (let ((tree (org-element-parse-buffer 'object)))
      (org-element-map
          tree
          'paragraph
        (lambda (el)
          (let (l)
            (setq l (add-to-list 'l (org-no-properties
                                     (car (org-element-contents el)))))

            l))))))

(defun tuedachu/parse-news-file (org-news-file html-file print-function filter-news? &optional preamble postamble)
  "Parse file ORG-NEWS-FILE and print content into HTML-FILE using function PRINT-FUNCTION.
PRINT-FUNCTION is a funciton taking two arguments (a header and a string) and returns a string.
A PREAMBLE and POSTAMBLE can be printed out in the html file."
  (let ((header-list (tuedachu/org-get-header-list org-news-file '(:raw-value
                                                                   :URL
                                                                   :DATE
                                                                   :DISPLAY
                                                                   :KEYWORDS)))
        (news-content (tuedachu/org-get-content-list org-news-file))
        current-header
        current-content
        (idx 0)
        str)
    (with-temp-file html-file
      (when preamble
        (insert (concat preamble
                        "\n\n")))
      (while header-list
        (setq current-header (car header-list)
              current-content (car (nth idx news-content))
              str (if filter-news?
                      (if (not (string= (nth 3 current-header) "nil"))
                          (funcall print-function
                                   current-header
                                   current-content)
                        nil)
                    (funcall print-function
                             current-header
                             current-content)))
        (when str
          (insert str))
        (setq header-list (cdr header-list)
              idx (1+ idx)))
      (when filter-news?
        (insert (concat "<a class='news_link' href='"
                        tuedachu/domain
                        "all_news.html"
                        "'> See all news...</a>\n<br><br>")))
      (when postamble
        (insert (concat "\n"
                        postamble))))))

(defun tuedachu/print-news-html(current-header content)
  (concat
   "<div class='ticket'>\n"
   "<h2>"
   (nth 0 current-header)
   "</h2>\n<p>"
   (mapconcat
    (lambda(line)
      (mapconcat 'identity
                 (split-string line
                               " "
                               t)
                 " "))
    (split-string content
                  "\n"
                  t)
    " ")
   "</p>\n"
   (when (nth 4 current-header)
     (concat "<p><b> Keywords: "
             (nth 4 current-header)))
   "</b></p>\n"
   (when (nth 1 current-header)
     (concat "<a class='news_link' href='"
             (nth 1 current-header)
             "'> More information...</a>\n"))
   "<p class='date'>Posted on: "
   (nth 2 current-header)
   "</p>\n\n</div>\n\n"))

(provide 'news-builder)
;;; news-builder.el ends here
